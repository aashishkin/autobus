﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autobus
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;
            Console.WriteLine("Кол-во мостов: ");
            int n = int.Parse(Console.ReadLine());
            for (i = 0; i < n; i++)
            {
                Console.WriteLine($"Высота моста №{i + 1}: ");
                if (int.Parse(Console.ReadLine()) <= 437)
                {
                    Console.WriteLine($"Crash {i+1}");
                    break;
                }
            }
            if (i == n) Console.WriteLine("No crash");
            Console.ReadKey();
        }
    }
}
